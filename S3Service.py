import boto3

BUCKET_NAME = 'trax-mobile-deadq'


class S3Service:
    def __init__(self, path):
        self.s3 = boto3.resource('s3')
        self.client = boto3.client('s3')
        self.bucket = self.s3.Bucket(BUCKET_NAME)
        self.path = path

    @property
    def file_keys(self):
        objects = self.bucket.objects.filter(Prefix=self.path)
        ids = []
        for obj in objects:
            ids.append(obj.key)
        return ids

    def download_file(self, key):
        name = key.split("/")[-1]
        with open(name, 'wb') as data:
            self.bucket.download_fileobj(key, data)
            data.close()
            return name

    def remove_files(self, keys):
        # type: (list[str]) -> list[str]
        objects = list(map((lambda x: {'Key': x}), keys))
        res = self.bucket.delete_objects(Delete={'Objects': objects})
        return res


