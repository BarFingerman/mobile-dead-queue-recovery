from pymongo import MongoClient


def get_api_key(project, env='prod'):
    host = 'trax-mongodb-prod01.trax-cloud.com'
    if env is 'int':
        host = 'trax-mongodb-integ01.trax-cloud.com'
    mongo = MongoClient(host, 27000)
    db = mongo['SmartInt']
    db.authenticate(name='engine', password='traxEngine')
    api_keys = db.api_keys
    print(api_keys)
    project_key = api_keys.find_one({"project_name": project})['key']
    return project_key

