import os.path
from S3Service import S3Service
from Upload import UploadService
from FileResource import FileResource
from Fixes import *
from MobileServerAuth import *


class Recovery:

    def __init__(self, project, env, platform, root_folder='', path = None):
        # type: (str, str, str, str) -> Recovery
        self.project = project
        self.env = env
        self.platform = platform
        self.root_folder = root_folder
        self.file_keys = []
        self.path = path if path is not None else self.build_path
        self.s3 = S3Service(path=self.path)
        token = get_api_key(project, env)
        self.mobile_server = UploadService(base_url=self.base_url, token=token)
        self.downloaded = dict()
        self.uploaded = []
        #self.path = path


    @property
    def build_path(self):

        if len(self.root_folder) > 0:
            root = self.root_folder + '/'
        else:
            root = ''
        return '{0}{1}/{2}/{3}/'.format(root, self.env, self.project, self.platform)

    @property
    def base_url(self):
        if self.env == 'int':
            return 'https://services-{0}.traxretail.com/api/v4.1/{1}'.format(self.env, self.project)
        else:
            return 'https://services.traxretail.com/api/v4.1/{0}'.format(self.project)

    def fetch(self):
        files = self.s3.file_keys
        self.file_keys = files[0:10] #list(filter(lambda x: 'syrveyr' in x, files))[0:100]
        return files

    def file_resource(self, file_name):
        # type: (str) -> FileResource
        if self.platform is 'mobile_android':
            return FileResource.android_resource(file_name)
        if 'ios' in self.platform:
            return FileResource.ios_resource(file_name)
        return None

    def download(self):
        count = 0
        for file_key in self.file_keys:
            print()
            print('download: {0} {1}'.format(file_key, count))
            file_name = self.s3.download_file(file_key)
            if file_name is not None:
                self.downloaded[file_name] = file_key
                count += 1
        return count

    def upload(self):
        count = 0
        for file_name, file_key in self.downloaded.items():
            print('upload file: {0} {1} started'.format(file_name, count))
            resource = self.file_resource(file_name)
            print('resource: {0}'.format(resource))
            if resource and UploadService.can_upload(resource):
                print('upload: {0}'.format(resource))
                result = self.mobile_server.upload(resource)
                if result[0]:
                    print('upload succeeded')
                    count += 1
                else:
                    print('upload failed')
                print(result[3], result[1], result[2])
                if should_remove(result[1], result[2]):
                    self.uploaded.append(file_key)
                    os.remove(file_name)
                    print('will remove from s3')
                else:
                    print('file is not fixed')
            else:
                print('cannot upload file: {0} '.format(file_name))
            #os.remove(file_name)
            print('file: {0} {1} completed'.format(file_name, count))
        return count

    def delete(self):
        if len(self.uploaded) > 0:
            result = self.s3.remove_files(self.uploaded)
            print(result)
            return result

    def upload_dummy(self):
        unknown_files = dict()
        count = 0
        for file_name, file_key in self.downloaded.items():
            print('upload file: {0} {1} started'.format(file_name, count))
            resource = self.file_resource(file_name)
            if resource and UploadService.can_upload(resource):
                print('resource: {0}'.format(resource))
            else:
                with open(file_name, 'r') as file:
                    unknown_files[file_name] = file.read()
                print('unknown file: {0}'.format(file_name))
            os.remove(file_name)
        return unknown_files

    def start(self):

        self.fetch()
        self.download()
        #res = self.upload_dummy()
        #print(res)
        self.upload()
        self.delete()
