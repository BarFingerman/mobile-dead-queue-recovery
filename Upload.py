from FileResource import FileResource
import requests


class UploadService:

    def __init__(self, base_url, token):
        self.base_url = base_url
        self.auth_token = token


    @staticmethod
    def can_upload(resource):
        # type: (FileResource) -> Bool
        if resource.rest and resource.content_type and resource.data:
            return True
        return False

    def upload(self, resource):
        # type: (FileResource)
        url = resource.url(base_url=self.base_url)
        headers = {'Authorization': 'Auth-Token {0}'.format(self.auth_token),
                   'mobile_platform': FileResource.platform,
                   'Content-Type': resource.content_type}
        resp = requests.post(url=url, data=resource.data, headers=headers)
        code = resp.status_code
        if code == 202 or code == 200 or code == 409:
            return True, code, resp.content, url
        else:
            return False, code, resp.content, url

