
from enum import Enum

BASE_URL = 'https://services.traxretail.com'

class Prefix(Enum):
    image = "i"
    surveyImage = "is"
    surveyTask = "sut"
    scene = "snt"
    session = "s"
    report = "r"
    form = "f"
    taskPosition = "tp"
    visitFeedback = "vf"
    deletedImage = "di"
    canceledScene = "csnt"
    canceledSession = "cs"
    sceneRejected = "scener"
    sessionRejected = "sessionr"
    imageRejected = "imager"
    surveyImageRejected = "simager"
    surveyRejected = "syrveyr"
    deviceToken = "dt"
    unknown = "unknown"



class FileResource:

    platform  = 'mobile_ios'

    def __init__(self, file_name, prefix, version, uid, scene = None, session = None):
        # type: (str, str, Prefix, str, str, str)
        self.prefix = prefix
        self.api_version = version
        self.uid = uid
        self.scene = scene
        self.session = session
        self.file_name = file_name
        with open(self.file_name, 'rb') as data:
            self.data = data.read()
            data.close()

    def __repr__(self):
        return "<FileResource {0} api_ver:{1} uid:{2} scene: {3} session: {4}>".format(self.prefix,
                                                                                       self.api_version,
                                                                                       self.uid,
                                                                                       self.scene,
                                                                                       self.session)

    @property
    def rest(self):
        if self.prefix in [Prefix.image, Prefix.imageRejected, Prefix.deletedImage]:
            return '/session/{0}/scene/{1}/image'.format(self.session, self.scene)
        if self.prefix in [Prefix.surveyImage, Prefix.surveyImageRejected]:
            return '/session/{0}/questionnaire/{1}/questionnaire_image'.format(self.session, self.scene)
        if self.prefix in [Prefix.scene, Prefix.sceneRejected, Prefix.canceledScene]:
            return '/session/{0}/scene'.format(self.scene)
        if self.prefix in [Prefix.session, Prefix.sessionRejected, Prefix.canceledSession]:
            return '/session'
        if self.prefix in [Prefix.surveyRejected, Prefix.surveyTask]:
            return '/user-response'
        if self.prefix is Prefix.taskPosition:
            return '/store-task-list'
        if self.prefix is Prefix.visitFeedback:
            return  'session/{0}/end-visit-feedback'.format(self.session)
        return None

    @property
    def content_type(self):
        if self.prefix in [Prefix.image, Prefix.imageRejected, Prefix.surveyImage, Prefix.surveyImageRejected]:
            if FileResource.platform == 'mobile_android':
                return 'multipart/form-data; boundary=+++++'
            else:
                return 'multipart/form-data; boundary={0}'.format(self.uid)
        return 'application/json'

    @staticmethod
    def android_resource(file_name):
        FileResource.platform = 'mobile_android'
        comp = file_name.split('_')
        length = len(comp)
        if length < 9:
            return None
        prefix = Prefix(comp[0])
        image = comp[5]
        session = comp[3]
        scene = comp[4]
        version = comp[8]
        if prefix in [Prefix.image, Prefix.imageRejected,
                      Prefix.surveyImage, Prefix.surveyImageRejected,
                      Prefix.deletedImage]:
            return FileResource(file_name, prefix, version, image, scene=scene, session=session)
        if prefix in [Prefix.surveyTask, Prefix.surveyRejected, Prefix.scene, Prefix.canceledScene, Prefix.sceneRejected]:
            return FileResource(file_name, prefix, version, scene, scene=scene, session=comp[3])
        if prefix in [Prefix.session, Prefix.sessionRejected, Prefix.canceledSession]:
            return FileResource(file_name, prefix, version, session, scene=None, session=session)
        if prefix is Prefix.unknown and session is 'N' and scene is 'N' and image is 'N':
            return FileResource(file_name, Prefix.taskPosition, version,session=None, scene=None, uid=comp[1])
        if prefix is Prefix.unknown and session is not 'N' and scene is 'N' and image is 'N':
            return FileResource(file_name, Prefix.visitFeedback, version, session=session, scene=None, uid=session)
        return None

    @staticmethod
    def ios_resource(file_name):
        FileResource.platform = 'mobile_ios'
        comp = file_name.split('_')
        length = len(comp)
        if length < 3:
            return None
        prefix = Prefix(comp[0])
        version = comp[1]
        uid = comp[2]
        if prefix in [Prefix.image, Prefix.imageRejected,
                      Prefix.surveyImage, Prefix.surveyImageRejected,
                      Prefix.deletedImage] and length == 5:
            return FileResource(file_name, prefix, version, uid, scene=comp[4], session=comp[3])
        if prefix in [Prefix.surveyTask, Prefix.surveyRejected, Prefix.scene, Prefix.canceledScene, Prefix.sceneRejected] and length > 3:
            return FileResource(file_name, prefix, version, uid, scene=uid, session=comp[3])
        if prefix in [Prefix.session, Prefix.sessionRejected, Prefix.canceledSession]:
            return FileResource(file_name, prefix, version, uid, scene=None, session=uid)
        return None

    def url(self, base_url):
        new_base_url = base_url
        if self.prefix in [Prefix.surveyTask,
                    Prefix.surveyRejected,
                    Prefix.visitFeedback]:
            new_base_url = new_base_url.replace('api', 'mobile')
        url = new_base_url + self.rest
        return url

