

def should_remove(code, body):
    if code == 202:
        return True
    if code == 200:
        return True
    if code == 409:
        return True
    if code == 400:
        if body.decode("utf-8") == '{"err":"unknown store"}':
            return False
    return False
